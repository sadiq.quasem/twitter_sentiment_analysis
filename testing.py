import os
import numpy as np
import pandas as pd
import tensorflow as tf
import tensorflow_hub as hub
import json
import urllib

from sklearn.preprocessing import MultiLabelBinarizer


data = pd.read_csv(
    'https://www.figure-eight.com/wp-content/uploads/2016/03/Apple-Twitter-Sentiment-DFE.csv', encoding="ISO-8859-1")
data['text'] = data['text'].str.lower()
data = data[data.sentiment.str.contains("not_relevant") == False]
data['contains_url'] = data['text'].str.contains('http').astype(int)
profanity = pd.read_fwf(
    'https://www.cs.cmu.edu/~biglou/resources/bad-words.txt', header=None)
words = [any(i in words for i in profanity[0].values)
         for words in data['text'].str.split().values]
words = np.array(words, dtype=np.float32)
data['contains_profanity'] = words.astype(int)
sentiment = data['sentiment']
text = data['text']
url = data['contains_url']
profanity = data['contains_profanity']


feature_spec = {
    "text": tf.placeholder(dtype=tf.string, shape=[None]),
    "url": tf.placeholder(dtype=tf.bool, shape=[None]),
    "profanity": tf.placeholder(dtype=tf.bool, shape=[None]),
}
tf.estimator.export.ServingInputReceiver(feature_spec, feature_spec)

train_size = int(len(text) * .8)
train_text = text[:train_size]
train_sentiment = sentiment[:train_size]
train_url = url[:train_size]
train_profanity = profanity[:train_size]

test_text = text[train_size:]
test_sentiment = sentiment[train_size:]
test_url = url[train_size:]
test_profanity = profanity[train_size:]

text_embeddings = hub.text_embedding_column(
    "text",
    module_spec="https://tfhub.dev/google/elmo/2"
)

encoder = MultiLabelBinarizer()
encoder.fit_transform(train_sentiment)
train_encoded = encoder.transform(train_sentiment)
test_encoded = encoder.transform(test_sentiment)
num_classes = len(encoder.classes_)

multi_label_head = tf.contrib.estimator.multi_label_head(
    num_classes,
    loss_reduction=tf.losses.Reduction.SUM_OVER_BATCH_SIZE
)

estimator = tf.contrib.estimator.DNNEstimator(
    head=multi_label_head,
    hidden_units=[16, 10],
    feature_columns=[text_embeddings],
)

# Format our data for the numpy_input_fn
features = {
    "text": np.array(train_text),
    "url": np.array(train_url),
    "profanity": np.array(train_profanity)
}
labels = np.array(train_encoded)

train_input_fn = tf.estimator.inputs.numpy_input_fn(
    features,
    labels,
    shuffle=True,
    batch_size=32,
    num_epochs=10
)
