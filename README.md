# Twitter Sentiment Analysis

The goal of this project is to set up an end to end machine learning pipeline that  predicts Twitter sentiment, using Tensorflow Hub and Tensorflow Serving. The model is trained on the Apple Computers Twitter Dataset

https://www.figure-eight.com/wp-content/uploads/2016/03/Apple-Twitter-Sentiment-DFE.csv

The pipeline does the following

1. Reads the dataset into memory
2. Computes ELMO word embeddings using TensorFlow hub: https://tfhub.dev/google/elmo/2
3. Computes 2 engineered features: contains_url and contains_profanity
4. Merges the ELMo embeddings and the 2 engineered features
5. Trains any classifier to predict the `sentiment` class
6. Freezes the graph
7. Expose the trained model in TensorFlow Serving `TO DO`

###### This project assumes you have the following:

```
1. Anaconda Python Distribution
2. Docker
```

After cloning the repo, please create a conda environment to make sure you have all the libraries needed to run the code. For your convenience, an `env.yml` file has been provided. Run it using:
```
conda env create -f env.yml
```
1. Train the model (or use pretrained model). You can change the steps and output paths as desired. Please note that this model takes a while to train
```
python train.py --steps 100 --saved_dir ./models/ --model_dir ./elmo_ckpt
```
2. After the model is trained you can serve predictions through the `predict.py` script. Please make changes to the script as necessary for your models:
```
-On line 7 and 8, change the path to your model checkpoint files
-Inside the raw_test array, you can add multiple tweets for prediction, seperated by commas
```
3. In the interest of time, I have provided a trained model. Run the following code for predictions:
```
python predict.py
```

Please note that this is a project in progress, and the prediction pipeline was a fast prototype of what the end product should be. Tensorflow Hub has some issues serving predictions through Tensorflow Serving, as soon as I am able to work around it, the pipeline for prediction will use Tensorflow Serving instead. The prototype pipeline will be deprecated at that time.
