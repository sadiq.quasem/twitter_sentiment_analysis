import tensorflow as tf
import os
import numpy as np
from testing import *

'''
Loads model into Session
'''

with tf.Session() as sess:
    new_saver = tf.train.import_meta_graph(
        './elmo_ckpt/model.ckpt-2958.meta', clear_devices=True)
    new_saver.restore(sess, './elmo_ckpt/model.ckpt-2958')

'''
Text for predictions
'''
raw_test = [
    "RT @MMLXIV: there is no avocado emoji may I ask why @apple",
]

'''
Uses estimator prediction function against the model
'''

predict_input_fn = tf.estimator.inputs.numpy_input_fn(
    {"text": np.array(raw_test).astype(np.str)}, shuffle=False)
results = estimator.predict(predict_input_fn)


'''
Outputs results with probabilities
- Can be modified to show top (n) results for models with more classes
'''
for i in results:
    top = i['probabilities'].argsort()[-1:][::-1]
    for j in top:
        text_sentiment = encoder.classes_[j]
        print(text_sentiment + ': ' +
              str(round(i['probabilities'][j] * 100, 2)) + '%')
    print('')
